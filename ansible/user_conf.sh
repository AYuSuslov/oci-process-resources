#!/bin/env bash

set -eu

: "${UID:=0}"
: "${GID:=${UID}}"

if [[ "${UID}" != "0" ]] ; then
    usermod -u "${UID}" "${USERNAME}" 2>/dev/null && {
        groupmod -g "${GID}" "${USERNAME}" 2>/dev/null ||
        usermod -a -G "${GID}" "${USERNAME}"
    }
    chown "${USERNAME}":"${USERNAME}" ../
    su "${USERNAME}" -c "ansible-playbook ${*}"
fi

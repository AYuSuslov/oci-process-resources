#!/usr/bin/env bash

set -euxo pipefail
SHOWOCI_ARGS="-ani -cp $COMPARTMENT -rg $REGION -csv result/all -t $OCI_CONFIG_PROFILE"
PROCESS_ARGS="--dir result --profile $OCI_CONFIG_PROFILE --log result/report.log"

if [ -n "${DELETE_CLUSTERS}" ] ; then
  PROCESS_ARGS+=" --clusters ${DELETE_CLUSTERS}"
fi

python showoci.py ${SHOWOCI_ARGS} | tee -a result/report.log
python oci_process_resources.py ${PROCESS_ARGS}

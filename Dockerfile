FROM python:3.10-slim
ARG USERNAME
ENV USERNAME=oci
RUN apt-get update && apt-get install -y wget && apt-get clean && python -m pip install --upgrade pip oci oci-cli ansible openpyxl && \
    ansible-galaxy collection install -p /usr/local/lib/python3.10/site-packages/ oracle.oci && \
    rm -rf /var/lib/apt/lists/* /root/.cache/* && groupadd "$USERNAME" && useradd -ms /bin/bash "$USERNAME" -g "$USERNAME"
USER ${USERNAME}
RUN  mkdir /home/${USERNAME}/ansible /home/${USERNAME}/shell /home/${USERNAME}/showoci && \
    wget https://raw.githubusercontent.com/oracle/oci-python-sdk/master/examples/showoci/showoci.py -O /home/${USERNAME}/showoci/showoci.py && \
    wget https://raw.githubusercontent.com/oracle/oci-python-sdk/master/examples/showoci/showoci_data.py -O /home/${USERNAME}/showoci/showoci_data.py && \
    wget https://raw.githubusercontent.com/oracle/oci-python-sdk/master/examples/showoci/showoci_service.py -O /home/${USERNAME}/showoci/showoci_service.py && \
    wget https://raw.githubusercontent.com/oracle/oci-python-sdk/master/examples/showoci/showoci_output.py -O /home/${USERNAME}/showoci/showoci_output.py
COPY --chown=${USERNAME}:${USERNAME} ./ansible/ /home/${USERNAME}/ansible/
COPY --chown=${USERNAME}:${USERNAME} ./python /home/${USERNAME}/showoci/
COPY --chown=${USERNAME}:${USERNAME} ./oci_run.sh /home/${USERNAME}/shell/
WORKDIR /home/${USERNAME}/ansible
USER root
ENTRYPOINT ["./user_conf.sh"]

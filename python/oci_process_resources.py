import sys
import os
import csv
import argparse
from openpyxl import Workbook
from openpyxl.utils import get_column_letter


def column_autofit(ws):
    dims = {}
    for row in ws.rows:
        for cell in row:
            if cell.value:
                dims[cell.column] = max((dims.get(cell.column, 0), len(str(cell.value))))
    for col, value in dims.items():
        ws.column_dimensions[get_column_letter(col)].width = value + 2
    return ws


def add_csv_to_workbook(wb, csv_file, csv_dir):
    file = os.path.join(csv_dir, csv_file)
    base_filename = os.path.splitext(csv_file)[0]
    part_name = '_'.join(base_filename.split('_')[0:-1])
    list_name = part_name[0:(30 - len(base_filename.split('_')[-1]))] + '_' + base_filename.split('_')[-1]
    wb.create_sheet(title=list_name)
    ws = wb[list_name]
    with open(file) as f_name:
        reader = csv.reader(f_name, delimiter=',')
        for row in reader:
            ws.append(row)
    c = ws['A2']
    ws.freeze_panes = c
    max_column_letter = get_column_letter(ws.max_column)
    ws.auto_filter.ref = 'A1:' + max_column_letter + str(len(ws['A']))
    column_autofit(ws)
    return wb


def process_sheet(ws, c_sheet, i_sheet, add_cluster_names, cl_comp):
    ws.insert_cols(1, amount=1)
    ws['A1'].value = 'end_user'
    if add_cluster_names:
        ws.insert_cols(2, amount=1)
        ws['B1'].value = 'cluster_name'
    freeform_tags_column = 0
    created_by_column = 0
    instance_id_column = 0
    cluster_id_column = 0
    compartment_name_column = 0
    region_name_column = 0
    compartment_id_column = 0
    instance_ids = []
    cluster_ids = []
    cluster_names = []
    for c in range(1, ws.max_column + 1):
        column_name = ws.cell(row=1, column=c).value
        if ws.cell(row=1, column=c).value == 'Tag_Oracle-Tags.CreatedBy':
            created_by_column = c
        if ws.cell(row=1, column=c).value == 'freeform_tags' and add_cluster_names:
            freeform_tags_column = c
        if (ws.cell(row=1, column=c).value == 'compartment_name' or
            ws.cell(row=1, column=c).value == 'compartment') and add_cluster_names:
            compartment_name_column = c
        if ws.cell(row=1, column=c).value == 'region_name' and add_cluster_names:
            region_name_column = c
    if ws.title.split('_')[-1] == 'compartments':
        for c in range(1, ws.max_column + 1):
            if ws.cell(row=1, column=c).value == 'name':
                compartment_name_column = c
            if ws.cell(row=1, column=c).value == 'id':
                compartment_id_column = c
    if i_sheet:
        for c in range(1, i_sheet.max_column + 1):
            if i_sheet.cell(row=1, column=c).value == 'instance_id':
                instance_id_column = c
    if c_sheet:
        for c in range(1, c_sheet.max_column + 1):
            if c_sheet.cell(row=1, column=c).value == 'id':
                cluster_id_column = c
    if instance_id_column != 0:
        instance_ids = [i_sheet.cell(row=i, column=instance_id_column).value for i in range(2, i_sheet.max_row + 1)]
    if cluster_id_column != 0:
        cluster_ids = [c_sheet.cell(row=i, column=cluster_id_column).value for i in range(2, c_sheet.max_row + 1)]
        cluster_names = [c_sheet.cell(row=i, column=2).value for i in range(2, c_sheet.max_row + 1)]
    for r in range(2, ws.max_row + 1):
        if freeform_tags_column != 0:
            ff_tags = str(ws.cell(row=r, column=freeform_tags_column).value).split(', ')
            if ff_tags:
                cn_tag = [tag for tag in ff_tags if 'cluster-name' in tag]
                if cn_tag:
                    ws.cell(row=r, column=2).value = cn_tag[0].split('=')[1]
        if created_by_column != 0:
            if 'ooma.com' in str(ws.cell(row=r, column=created_by_column).value):
                ws.cell(row=r, column=1).value = ws.cell(row=r, column=created_by_column).value
            elif 'ocid1.instance' in str(ws.cell(row=r, column=created_by_column).value):
                try:
                    owners_row = instance_ids.index(ws.cell(row=r, column=created_by_column).value) + 2
                except ValueError:
                    owners_row = 0
                if owners_row != 0:
                    if ws == i_sheet:
                        ws.cell(row=r, column=1).value = i_sheet.cell(row=owners_row, column=created_by_column).value
                    else:
                        ws.cell(row=r, column=1).value = i_sheet.cell(row=owners_row, column=1).value
                        if not ws.cell(row=r, column=2).value:
                            ws.cell(row=r, column=2).value = i_sheet.cell(row=owners_row, column=2).value
                else:
                    ws.cell(row=r, column=1).value = ws.cell(row=r, column=created_by_column).value
            elif 'ocid1.cluster' in str(ws.cell(row=r, column=created_by_column).value):
                try:
                    owners_row = cluster_ids.index(ws.cell(row=r, column=created_by_column).value) + 2
                except ValueError:
                    owners_row = 0
                if owners_row != 0:
                    ws.cell(row=r, column=1).value = c_sheet.cell(row=owners_row, column=1).value
                    if not ws.cell(row=r, column=2).value:
                        ws.cell(row=r, column=2).value = c_sheet.cell(row=owners_row, column=2).value
                else:
                    ws.cell(row=r, column=1).value = ws.cell(row=r, column=created_by_column).value
            elif str(ws.cell(row=r, column=created_by_column).value) == 'oke':
                try:
                    owners_row = cluster_names.index(ws.cell(row=r, column=2).value) + 2
                except ValueError:
                    owners_row = 0
                if owners_row != 0:
                    ws.cell(row=r, column=1).value = c_sheet.cell(row=owners_row, column=1).value
                else:
                    ws.cell(row=r, column=1).value = ws.cell(row=r, column=created_by_column).value
        if compartment_name_column != 0:
            if ws.title.split('_')[-1] == 'compartments':
                cl_comp[ws.cell(row=r, column=compartment_name_column).value] =\
                    ws.cell(row=r, column=compartment_id_column).value
            else:
                if ws.cell(row=r, column=2).value:
                    cl_comp[ws.cell(row=r, column=2).value] = \
                        [ws.cell(row=r, column=compartment_name_column).value,
                         ws.cell(row=r, column=region_name_column).value]
    ws = column_autofit(ws)
    return ws, cl_comp


def process_sheet_by_vcn(ws, v_sheet):
    created_by_vcn_column = 0
    vcn_id_column = 0
    vcn_ids = []
    for c in range(1, ws.max_column + 1):
        if ws.cell(row=1, column=c).value == 'vcn_id':
            created_by_vcn_column = c
    if v_sheet:
        for c in range(1, v_sheet.max_column + 1):
            if v_sheet.cell(row=1, column=c).value == 'vcn_id':
                vcn_id_column = c
    if vcn_id_column != 0:
        vcn_ids = [v_sheet.cell(row=i, column=vcn_id_column).value for i in range(2, v_sheet.max_row + 1)]
    for r in range(2, ws.max_row + 1):
        if created_by_vcn_column != 0:
            try:
                owners_row = vcn_ids.index(ws.cell(row=r, column=created_by_vcn_column).value) + 2
            except ValueError:
                owners_row = 0
            if owners_row != 0:
                if not ws.cell(row=r, column=1).value:
                    ws.cell(row=r, column=1).value = v_sheet.cell(row=owners_row, column=1).value
                if not ws.cell(row=r, column=2).value:
                    ws.cell(row=r, column=2).value = v_sheet.cell(row=owners_row, column=2).value
    ws = column_autofit(ws)
    return ws


def delete_instance_commands(d_sheet, i_sheet, c_name):
    instance_id_column = 0
    instance_name_column = 0
    freeform_tags_column = 0
    region_column = 0
    master_pool = ()
    i_pools = []
    i_configs = []
    instances = []
    for c in range(1, i_sheet.max_column + 1):
        if i_sheet.cell(row=1, column=c).value == 'region_name':
            region_column = c
        if i_sheet.cell(row=1, column=c).value == 'instance_id':
            instance_id_column = c
        if i_sheet.cell(row=1, column=c).value == 'freeform_tags':
            freeform_tags_column = c
        if i_sheet.cell(row=1, column=c).value == 'server_name':
            instance_name_column = c
    for i in range(2, len(i_sheet['B']) + 1):
        if i_sheet.cell(row=i, column=2).value == c_name:
            ff_tags = str(i_sheet.cell(row=i, column=freeform_tags_column).value).split(', ')
            region = i_sheet.cell(row=i, column=region_column).value
            if ff_tags:
                i_pool = [tag for tag in ff_tags if 'oci:compute:instancepool=' in tag]
                i_config = [tag for tag in ff_tags if 'oci:compute:instanceconfiguration' in tag]
                if i_pool:
                    if 'master' in i_sheet.cell(row=i, column=instance_name_column).value and not master_pool:
                        master_pool = (region, i_pool[0].split('=')[1])
                    elif master_pool != (region, i_pool[0].split('=')[1]):
                        i_pools.append((region,  i_pool[0].split('=')[1]))
                if i_config:
                    i_configs.append((region, i_config[0].split('=')[1]))
                if not (i_pool or i_config):
                    instances.append((region, i_sheet.cell(row=i, column=instance_id_column).value,
                                      i_sheet.cell(row=i, column=instance_name_column).value))
    if master_pool:
        command = 'oci compute-management instance-pool terminate --region ' + master_pool[0] + \
                  ' --force --instance-pool-id ' + master_pool[1]
        d_sheet.cell(row=(len(d_sheet['A']) + 1), column=1).value = command
    for i_pool in set(i_pools):
        command = 'oci compute-management instance-pool terminate --region ' + i_pool[0] +\
                  ' --force --instance-pool-id ' + i_pool[1]
        d_sheet.cell(row=(len(d_sheet['A']) + 1), column=1).value = command
    for i_config in set(i_configs):
        command = 'oci compute-management instance-configuration delete --region ' + i_config[0] + \
                  ' --force --instance-configuration-id ' + i_config[1]
        d_sheet.cell(row=(len(d_sheet['A']) + 1), column=1).value = command
    for instance in set(instances):
        command = 'oci compute instance terminate --region ' + instance[0] + \
                  ' --force --instance-id ' + instance[1]
        new_row = len(d_sheet['A']) + 1
        d_sheet.cell(row=new_row, column=1).value = command
        d_sheet.cell(row=new_row, column=2).value = instance[2]
    return d_sheet


def delete_volumes_commands(d_sheet, v_sheet, c_name):
    bv_id_column = 0
    instance_name_column = 0
    region_column = 0
    block_volumes = []
    for c in range(1, v_sheet.max_column + 1):
        if v_sheet.cell(row=1, column=c).value == 'region_name':
            region_column = c
        if v_sheet.cell(row=1, column=c).value == 'bv_id':
            bv_id_column = c
        if v_sheet.cell(row=1, column=c).value == 'instance_name':
            instance_name_column = c
    for i in range(2, len(v_sheet['B']) + 1):
        if v_sheet.cell(row=i, column=2).value == c_name and bv_id_column != 0:
            region = v_sheet.cell(row=i, column=region_column).value
            if '.volume.' in v_sheet.cell(row=i, column=bv_id_column).value:
                block_volumes.append((region, v_sheet.cell(row=i, column=bv_id_column).value,
                                      v_sheet.cell(row=i, column=instance_name_column).value))
    for bv in set(block_volumes):
        command = 'oci bv volume delete --region ' + bv[0] +\
                  ' --force --volume-id ' + bv[1]
        new_row = len(d_sheet['A']) + 1
        d_sheet.cell(row=new_row, column=1).value = command
        d_sheet.cell(row=new_row, column=2).value = bv[2]
    return d_sheet


def delete_balancers_commands(d_sheet, b_sheet, c_name):
    lb_id_column = 0
    region_column = 0
    balancers = []
    for c in range(1, b_sheet.max_column + 1):
        if b_sheet.cell(row=1, column=c).value == 'region_name':
            region_column = c
        if b_sheet.cell(row=1, column=c).value == 'loadbalancer_id':
            lb_id_column = c
    for i in range(2, len(b_sheet['B']) + 1):
        if b_sheet.cell(row=i, column=2).value == c_name:
            region = b_sheet.cell(row=i, column=region_column).value
            balancers.append((region, b_sheet.cell(row=i, column=lb_id_column).value))
    for lb in set(balancers):
        command = 'oci lb load-balancer delete --region ' + lb[0] +\
                  ' --force --load-balancer-id ' + lb[1]
        d_sheet.cell(row=(len(d_sheet['A']) + 1), column=1).value = command
    return d_sheet


def delete_drgs_commands(d_sheet, drg_sheet, c_name, c_comp, profile):
    from oci.config import from_file
    from oci.core import VirtualNetworkClient
    from oci.exceptions import ServiceError
    oci_config = from_file(profile_name=profile)
    drg_id_column = 0
    region_column = 0
    drgs = []
    for c in range(1, drg_sheet.max_column + 1):
        if drg_sheet.cell(row=1, column=c).value == 'region_name':
            region_column = c
        if drg_sheet.cell(row=1, column=c).value == 'id':
            drg_id_column = c
    for i in range(2, len(drg_sheet['B']) + 1):
        if drg_sheet.cell(row=i, column=2).value == c_name:
            region = drg_sheet.cell(row=i, column=region_column).value
            drgs.append((region, c_comp[0], drg_sheet.cell(row=i, column=drg_id_column).value))
    for drg in set(drgs):
        oci_config['region'] = drg[0]
        oci_core_client = VirtualNetworkClient(oci_config)
        try:
            list_rpc = oci_core_client.list_remote_peering_connections(compartment_id=drg[1], drg_id=drg[2]).data
        except ServiceError:
            list_rpc = []
        for rpc in list_rpc:
            command = 'oci network remote-peering-connection delete --region ' + drg[0] +\
                      ' --force --remote-peering-connection-id ' + rpc.id
            d_sheet.cell(row=(len(d_sheet['A']) + 1), column=1).value = command
        try:
            list_attachments = oci_core_client.list_drg_attachments(compartment_id=drg[1], drg_id=drg[2]).data
        except ServiceError:
            list_attachments = []
        for att in list_attachments:
            command = 'oci network drg-attachment delete --region ' + drg[0] + \
                      ' --force --drg-attachment-id ' + att.id
            d_sheet.cell(row=(len(d_sheet['A']) + 1), column=1).value = command
        command = 'oci network drg delete --region ' + drg[0] + ' --force --drg-id ' + drg[2]
        d_sheet.cell(row=(len(d_sheet['A']) + 1), column=1).value = command
    return d_sheet


def delete_dns_commands(d_sheet, c_name, c_comp, profile):
    from oci.config import from_file
    from oci.dns import DnsClient
    from oci.exceptions import ServiceError
    oci_config = from_file(profile_name=profile)
    oci_config['region'] = c_comp[1]
    oci_dns_client = DnsClient(oci_config)
    view_commands = []
    try:
        list_zones = oci_dns_client.list_zones(compartment_id=c_comp[0], scope='PRIVATE').data
    except ServiceError:
        list_zones = []
    for zone in list_zones:
        if (not zone.is_protected) and 'cluster-name' in zone.freeform_tags:
            if zone.freeform_tags['cluster-name'] == c_name:
                command = 'oci dns zone delete --region ' + c_comp[1] + \
                          ' --force --zone-name-or-id ' + zone.id
                d_sheet.cell(row=(len(d_sheet['A']) + 1), column=1).value = command
                command = 'oci dns view delete --region ' + c_comp[1] + \
                          ' --force --view-id ' + zone.view_id
                view_commands.append(command)
    try:
        list_resolvers = oci_dns_client.list_resolvers(compartment_id=c_comp[0]).data
    except ServiceError:
        list_resolvers = []
    for resolver in list_resolvers:
        if 'cluster-name' in resolver.freeform_tags:
            if resolver.freeform_tags['cluster-name'] == c_name:
                try:
                    list_endpoints = oci_dns_client.list_resolver_endpoints(resolver_id=resolver.id).data
                except ServiceError:
                    list_endpoints = []
                for endpoint in list_endpoints:
                    if endpoint.name == 'forwarder' or endpoint.name == 'listener':
                        command = 'oci dns resolver-endpoint delete --region ' + c_comp[1] + \
                                  ' --force --resolver-endpoint-name ' + endpoint.name + \
                                  ' --resolver-id ' + resolver.id
                        d_sheet.cell(row=(len(d_sheet['A']) + 1), column=1).value = command
    try:
        list_views = oci_dns_client.list_views(compartment_id=c_comp[0]).data
    except ServiceError:
        list_views = []
    for view in list_views:
        if (not view.is_protected) and 'cluster-name' in view.freeform_tags:
            if view.freeform_tags['cluster-name'] == c_name:
                command = 'oci dns view delete --region ' + c_comp[1] + \
                          ' --force --view-id ' + view.id
                view_commands.append(command)
    return d_sheet, set(view_commands)


def delete_subnets_commands(d_sheet, sn_sheet, c_name):
    subnet_id_column = 0
    region_column = 0
    subnets = []
    for c in range(1, sn_sheet.max_column + 1):
        if sn_sheet.cell(row=1, column=c).value == 'region_name':
            region_column = c
        if sn_sheet.cell(row=1, column=c).value == 'subnet_id':
            subnet_id_column = c
    for i in range(2, len(sn_sheet['B']) + 1):
        if sn_sheet.cell(row=i, column=2).value == c_name:
            region = sn_sheet.cell(row=i, column=region_column).value
            subnets.append((region, sn_sheet.cell(row=i, column=subnet_id_column).value))
    for sn in set(subnets):
        command = 'oci network subnet delete --region ' + sn[0] +\
                  ' --force --subnet-id ' + sn[1]
        d_sheet.cell(row=(len(d_sheet['A']) + 1), column=1).value = command
    return d_sheet


def delete_route_tables_commands(d_sheet, rt_sheet, c_name):
    route_id_column = 0
    route_name_column = 0
    region_column = 0
    route_tables = []
    for c in range(1, rt_sheet.max_column + 1):
        if rt_sheet.cell(row=1, column=c).value == 'region_name':
            region_column = c
        if rt_sheet.cell(row=1, column=c).value == 'route_id':
            route_id_column = c
        if rt_sheet.cell(row=1, column=c).value == 'route_name':
            route_name_column = c
    for i in range(2, len(rt_sheet['B']) + 1):
        if rt_sheet.cell(row=i, column=2).value == c_name:
            region = rt_sheet.cell(row=i, column=region_column).value
            route_tables.append((region, rt_sheet.cell(row=i, column=route_name_column).value,
                                 rt_sheet.cell(row=i, column=route_id_column).value))
    for rt in set(route_tables):
        if 'Default Route Table' not in rt[1]:
            command = 'oci network route-table delete --region ' + rt[0] + \
                      ' --force --rt-id ' + rt[2]
            d_sheet.cell(row=(len(d_sheet['A']) + 1), column=1).value = command
    return d_sheet


def delete_sec_groups_commands(d_sheet, nsg_sheet, c_name):
    sec_id_column = 0
    region_column = 0
    sec_groups = []
    for c in range(1, nsg_sheet.max_column + 1):
        if nsg_sheet.cell(row=1, column=c).value == 'region_name':
            region_column = c
        if nsg_sheet.cell(row=1, column=c).value == 'sec_id':
            sec_id_column = c
    for i in range(2, len(nsg_sheet['B']) + 1):
        if nsg_sheet.cell(row=i, column=2).value == c_name:
            region = nsg_sheet.cell(row=i, column=region_column).value
            sec_groups.append((region, nsg_sheet.cell(row=i, column=sec_id_column).value))
    for nsg in set(sec_groups):
        command = 'oci network nsg delete --region ' + nsg[0] +\
                  ' --force --nsg-id ' + nsg[1]
        d_sheet.cell(row=(len(d_sheet['A']) + 1), column=1).value = command
    return d_sheet


def delete_sec_lists_commands(d_sheet, sl_sheet, c_name):
    sec_id_column = 0
    sec_name_column = 0
    region_column = 0
    sec_lists = []
    for c in range(1, sl_sheet.max_column + 1):
        if sl_sheet.cell(row=1, column=c).value == 'region_name':
            region_column = c
        if sl_sheet.cell(row=1, column=c).value == 'sec_id':
            sec_id_column = c
        if sl_sheet.cell(row=1, column=c).value == 'sec_name':
            sec_name_column = c
    for i in range(2, len(sl_sheet['B']) + 1):
        if sl_sheet.cell(row=i, column=2).value == c_name:
            region = sl_sheet.cell(row=i, column=region_column).value
            sec_lists.append((region, sl_sheet.cell(row=i, column=sec_name_column).value,
                              sl_sheet.cell(row=i, column=sec_id_column).value))
    for sl in set(sec_lists):
        if 'Default Security List' not in sl[1]:
            command = 'oci network security-list delete --region ' + sl[0] + \
                      ' --force --security-list-id ' + sl[2]
            d_sheet.cell(row=(len(d_sheet['A']) + 1), column=1).value = command
    return d_sheet


def delete_vcns_commands(d_sheet, v_sheet, c_name, c_comp, profile):
    from oci.config import from_file
    from oci.core import VirtualNetworkClient
    from oci.exceptions import ServiceError
    oci_config = from_file(profile_name=profile)
    vcn_id_column = 0
    region_column = 0
    vcns = []
    for c in range(1, v_sheet.max_column + 1):
        if v_sheet.cell(row=1, column=c).value == 'region_name':
            region_column = c
        if v_sheet.cell(row=1, column=c).value == 'vcn_id':
            vcn_id_column = c
    for i in range(2, len(v_sheet['B']) + 1):
        if v_sheet.cell(row=i, column=2).value == c_name:
            region = v_sheet.cell(row=i, column=region_column).value
            vcns.append((region, c_comp[0], v_sheet.cell(row=i, column=vcn_id_column).value))
    for vcn in set(vcns):
        oci_config['region'] = vcn[0]
        oci_core_client = VirtualNetworkClient(oci_config)
        try:
            list_igws = oci_core_client.list_internet_gateways(compartment_id=vcn[1], vcn_id=vcn[2]).data
        except ServiceError:
            list_igws = []
        for igw in list_igws:
            if 'cluster-name' in igw.freeform_tags:
                if igw.freeform_tags['cluster-name'] == c_name:
                    command = 'oci network internet-gateway delete --region ' + vcn[0] + \
                              ' --force --ig-id ' + igw.id
                    d_sheet.cell(row=(len(d_sheet['A']) + 1), column=1).value = command
        try:
            list_ngws = oci_core_client.list_nat_gateways(compartment_id=vcn[1], vcn_id=vcn[2]).data
        except ServiceError:
            list_ngws = []
        for ngw in list_ngws:
            if 'cluster-name' in ngw.freeform_tags:
                if ngw.freeform_tags['cluster-name'] == c_name:
                    command = 'oci network nat-gateway delete --region ' + vcn[0] + \
                              ' --force --nat-gateway-id ' + ngw.id
                    d_sheet.cell(row=(len(d_sheet['A']) + 1), column=1).value = command
        command = 'oci network vcn delete --region ' + vcn[0] + ' --force --vcn-id ' + vcn[2]
        d_sheet.cell(row=(len(d_sheet['A']) + 1), column=1).value = command
    return d_sheet


def main():
    result_directory = 'result/'
    if not os.path.exists(result_directory):
        os.makedirs(result_directory)
    parser = argparse.ArgumentParser(
        description='Create and Process XLSX files from directory with OCI resources csv-s')
    parser.add_argument('--dir', metavar='CSV_DIRECTORY', type=str, required=True,
                        help='directory with csv files to add and process')
    parser.add_argument('--clusters', metavar='DELETE_CLUSTERS', nargs='*', type=str,
                        help='cluster names to delete OCI resources')
    parser.add_argument('--log', metavar='LOG_NAME', default=sys.stdout, type=argparse.FileType('a'),
                        help='log file name')
    parser.add_argument('--profile', metavar='TENANCY_PROFILE', default='DEFAULT', type=str,
                        help='config file section to use')
    args = parser.parse_args()
    csv_dir = args.dir
    try:
        files = [f for f in os.listdir(csv_dir) if os.path.isfile(os.path.join(csv_dir, f))]
    except FileNotFoundError as e:
        print("invalid directory: {}".format(e))
        return None
    base_filename = os.path.basename(os.path.normpath(csv_dir))
    workbook = Workbook()
    worksheet = workbook.active
    worksheet.title = 'delete_commands'
    for f in files:
        if os.path.splitext(f)[1] == '.csv':
            workbook = add_csv_to_workbook(workbook, f, csv_dir)
            args.log.write('file {} is added to analysis.\n'.format(f))
    all_sheets = workbook.sheetnames
    cluster_comps = dict()
    comp_ids = dict()
    containers = [sheet for sheet in all_sheets if sheet.split('_')[-1] == 'containers']
    instances = [sheet for sheet in all_sheets if sheet.split('_')[-1] == 'compute']
    vcns = [sheet for sheet in all_sheets if sheet.split('_')[-1] == 'vcn']
    containers_sheet = workbook[(containers[0])] if containers else None
    instance_sheet = workbook[(instances[0])] if instances else None
    vcns_sheet = workbook[(vcns[0])] if vcns else None
    if containers_sheet:
        containers_sheet, cluster_comps = process_sheet(containers_sheet, containers_sheet, instance_sheet,
                                                        True, cluster_comps)
    if instance_sheet:
        instance_sheet, cluster_comps = process_sheet(instance_sheet, containers_sheet, instance_sheet,
                                                      True, cluster_comps)
    if vcns_sheet:
        vcns_sheet, cluster_comps = process_sheet(vcns_sheet, containers_sheet, instance_sheet, True,
                                                  cluster_comps)
    volume_sheet = None
    volumes = [sheet for sheet in all_sheets if sheet.split('_')[-1] == 'volumes']
    if volumes:
        volume_sheet = workbook[(volumes[0])]
        volume_sheet, cluster_comps = process_sheet(volume_sheet, containers_sheet, instance_sheet, True,
                                                    cluster_comps)
    node_pools = [sheet for sheet in all_sheets if sheet.split('_')[-1] == 'nodepools']
    if node_pools:
        node_pools_sheet = workbook[(node_pools[0])]
        node_pools_sheet, cluster_comps = process_sheet(node_pools_sheet, containers_sheet, instance_sheet,
                                                        True, cluster_comps)
    health_checks = [sheet for sheet in all_sheets if sheet.split('_')[-1] == 'healthchecks']
    if health_checks:
        health_checks_sheet = workbook[(health_checks[0])]
        health_checks_sheet, cluster_comps = process_sheet(health_checks_sheet, containers_sheet, instance_sheet,
                                                           False, cluster_comps)
    compartments = [sheet for sheet in all_sheets if sheet.split('_')[-1] == 'compartments']
    if compartments:
        compartments_sheet = workbook[(compartments[0])]
        compartments_sheet, comp_ids = process_sheet(compartments_sheet, containers_sheet, instance_sheet,
                                                     False, comp_ids)
    listeners = [sheet for sheet in all_sheets if sheet.split('_')[-1] == 'listeners']
    if listeners:
        listeners_sheet = workbook[(listeners[0])]
        listeners_sheet, cluster_comps = process_sheet(listeners_sheet, containers_sheet, instance_sheet,
                                                       True, cluster_comps)
    balancers_sheet = None
    balancers = [sheet for sheet in all_sheets if sheet.split('_')[-1] == 'balancers']
    if balancers:
        balancers_sheet = workbook[(balancers[0])]
        balancers_sheet, cluster_comps = process_sheet(balancers_sheet, containers_sheet, instance_sheet,
                                                       True, cluster_comps)
    dhcp_options = [sheet for sheet in all_sheets if sheet.split('_')[-1] == 'options']
    if dhcp_options:
        dhcp_options_sheet = workbook[(dhcp_options[0])]
        dhcp_options_sheet, cluster_comps = process_sheet(dhcp_options_sheet, containers_sheet, instance_sheet,
                                                          True, cluster_comps)
        dhcp_options_sheet = process_sheet_by_vcn(dhcp_options_sheet, vcns_sheet)
    drgs_sheet = None
    drgs = [sheet for sheet in all_sheets if sheet.split('_')[-1] == 'drgs']
    if drgs:
        drgs_sheet = workbook[(drgs[0])]
        drgs_sheet, cluster_comps = process_sheet(drgs_sheet, containers_sheet, instance_sheet, True, cluster_comps)
    routes_sheet = None
    routes = [sheet for sheet in all_sheets if sheet.split('_')[-1] == 'routes']
    if routes:
        routes_sheet = workbook[(routes[0])]
        routes_sheet, cluster_comps = process_sheet(routes_sheet, containers_sheet, instance_sheet, True,
                                                    cluster_comps)
        routes_sheet = process_sheet_by_vcn(routes_sheet, vcns_sheet)
    sec_groups_sheet = None
    sec_groups = [sheet for sheet in all_sheets if sheet.split('_')[-1] == 'group']
    if sec_groups:
        sec_groups_sheet = workbook[(sec_groups[0])]
        sec_groups_sheet, cluster_comps = process_sheet(sec_groups_sheet, containers_sheet, instance_sheet,
                                                        True, cluster_comps)
        sec_groups_sheet = process_sheet_by_vcn(sec_groups_sheet, vcns_sheet)
    sec_lists_sheet = None
    sec_lists = [sheet for sheet in all_sheets if sheet.split('_')[-1] == 'list']
    if sec_lists:
        sec_lists_sheet = workbook[(sec_lists[0])]
        sec_lists_sheet, cluster_comps = process_sheet(sec_lists_sheet, containers_sheet, instance_sheet,
                                                       True, cluster_comps)
        sec_lists_sheet = process_sheet_by_vcn(sec_lists_sheet, vcns_sheet)
    subnets_sheet = None
    subnets = [sheet for sheet in all_sheets if sheet.split('_')[-1] == 'subnet']
    if subnets:
        subnets_sheet = workbook[(subnets[0])]
        subnets_sheet, cluster_comps = process_sheet(subnets_sheet, containers_sheet, instance_sheet,
                                                     True, cluster_comps)
    prv_ips = [sheet for sheet in all_sheets if sheet.split('_')[-1] == 'ips']
    if prv_ips:
        prv_ips_sheet = workbook[(prv_ips[0])]
        prv_ips_sheet, cluster_comps = process_sheet(prv_ips_sheet, containers_sheet, instance_sheet,
                                                     True, cluster_comps)
        prv_ips_sheet = process_sheet_by_vcn(prv_ips_sheet, vcns_sheet)
    buckets = [sheet for sheet in all_sheets if sheet.split('_')[-1] == 'buckets']
    if buckets:
        buckets_sheet = workbook[(buckets[0])]
        buckets_sheet, cluster_comps = process_sheet(buckets_sheet, containers_sheet, instance_sheet,
                                                     False, cluster_comps)
    loggings = [sheet for sheet in all_sheets if sheet.split('_')[-1] == 'loggings']
    if loggings:
        loggings_sheet = workbook[(loggings[0])]
        loggings_sheet, cluster_comps = process_sheet(loggings_sheet, containers_sheet, instance_sheet,
                                                      False, cluster_comps)
    if cluster_comps and comp_ids:
        for cluster in cluster_comps:
            cluster_comps[cluster][0] = comp_ids[cluster_comps[cluster][0]]
    new_file_name = result_directory + base_filename + '_processed.xlsx'
    delete_file_name = result_directory + base_filename + '_processed.txt'
    if args.clusters:
        delete_clusters = args.clusters
        delete_sheet = workbook['delete_commands']
        delete_sheet['A1'] = 'delete_commands:'
        delete_sheet['B1'] = 'instance_names:'
        for cluster in delete_clusters:
            if cluster in cluster_comps:
                view_commands = []
                if instance_sheet:
                    delete_sheet = delete_instance_commands(delete_sheet, instance_sheet, cluster)
                if volume_sheet:
                    delete_sheet = delete_volumes_commands(delete_sheet, volume_sheet, cluster)
                if balancers_sheet:
                    delete_sheet = delete_balancers_commands(delete_sheet, balancers_sheet, cluster)
                if drgs_sheet and comp_ids:
                    delete_sheet = delete_drgs_commands(delete_sheet, drgs_sheet, cluster, cluster_comps[cluster],
                                                        args.profile)
                if comp_ids:
                    delete_sheet, view_commands = delete_dns_commands(delete_sheet, cluster, cluster_comps[cluster],
                                                                      args.profile)
                if subnets_sheet:
                    delete_sheet = delete_subnets_commands(delete_sheet, subnets_sheet, cluster)
                if routes_sheet:
                    delete_sheet = delete_route_tables_commands(delete_sheet, routes_sheet, cluster)
                if sec_groups_sheet:
                    delete_sheet = delete_sec_groups_commands(delete_sheet, sec_groups_sheet, cluster)
                if sec_lists_sheet:
                    delete_sheet = delete_sec_lists_commands(delete_sheet, sec_lists_sheet, cluster)
                if vcns_sheet and comp_ids:
                    delete_sheet = delete_vcns_commands(delete_sheet, vcns_sheet, cluster, cluster_comps[cluster],
                                                        args.profile)
                if view_commands:
                    for command in view_commands:
                        delete_sheet.cell(row=(len(delete_sheet['A']) + 1), column=1).value = command
        delete_sheet = column_autofit(delete_sheet)
        with open(delete_file_name, 'w') as f:
            command = ('a ' * 7)
            for i in range(1, len(delete_sheet['A'])):
                command = delete_sheet['A' + str(i + 1)].value
                f.write(command + '\n')
        args.log.write("TXT file {} with OCI delete commands is created.\n  "
                       "Edit this file if needed to remove or add commands for Ansible delete playbook\n  "
                       "or proceed commands manually to delete resources from OCI.\n".format(delete_file_name))
    workbook.save(new_file_name)
    workbook.close()
    args.log.write("XLSX file {} is processed.\n".format(new_file_name))
    args.log.close()
    return


main()

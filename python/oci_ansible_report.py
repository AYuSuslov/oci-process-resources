import sys
import os
import argparse


def main():
    result_directory = 'result/'
    if not os.path.exists(result_directory):
        os.makedirs(result_directory)
    parser = argparse.ArgumentParser(
        description='Create and Process XLSX files from directory with OCI resources csv-s')
    parser.add_argument('--dir', metavar='CSV_DIRECTORY', type=str, required=True,
                        help='directory with csv files to add and process')
    parser.add_argument('--clusters', metavar='DELETE_CLUSTERS', nargs='*', type=str,
                        help='cluster names to delete OCI resources')
    parser.add_argument('--log', metavar='LOG_NAME', default=sys.stdout, type=argparse.FileType('a'),
                        help='log file name')
    args = parser.parse_args()
    csv_dir = args.dir
    base_filename = os.path.basename(os.path.normpath(csv_dir))
    delete_file_name = result_directory + base_filename + '_processed.txt'
    ansible_log_name = result_directory + 'ansible.log'
    if args.clusters:
        if os.path.isfile(ansible_log_name) and os.path.isfile(delete_file_name):
            with open(ansible_log_name) as f, open(delete_file_name) as a:
                delete_commands = a.read().splitlines()
                fatal_commands = []
                while True:
                    string = f.readline()
                    if not string:
                        break
                    if 'TASK' in string:
                        if 'ocid1.' in string:
                            resource = string[string.find('ocid1.'):string.find(']')]
                    if 'fatal' in string:
                        for command in delete_commands:
                            if resource in command:
                                fatal_commands.append(command)
                                break
            if fatal_commands:
                args.log.write('The following clean-up actions failed (find the OCI resources as command arguments):\n')
                print('The following clean-up actions failed (find the OCI resources as command arguments):')
                for command in fatal_commands:
                    args.log.write(command+'\n')
                    print(command)
            else:
                args.log.write('All planned OCI resources have been deleted.\n')
                print('All planned OCI resources have been deleted.')
        else:
            args.log.write('Some of the files from {} and {} are not found.\n'.format(delete_file_name,
                                                                                      ansible_log_name))
            print('Some of the files from {} and {} are not found.'.format(delete_file_name,
                                                                             ansible_log_name))
    return


main()

import sys
import os
import argparse


def main():
    result_directory = 'result/'
    if not os.path.exists(result_directory):
        os.makedirs(result_directory)
    parser = argparse.ArgumentParser(
        description='Create and Process XLSX files from directory with OCI resources csv-s')
    parser.add_argument('--dir', metavar='CSV_DIRECTORY', type=str, required=True,
                        help='directory with csv files to add and process')
    parser.add_argument('--clusters', metavar='DELETE_CLUSTERS', nargs='*', type=str,
                        help='cluster names to delete OCI resources')
    parser.add_argument('--log', metavar='LOG_NAME', default=sys.stdout, type=argparse.FileType('a'),
                        help='log file name')
    args = parser.parse_args()
    csv_dir = args.dir
    base_filename = os.path.basename(os.path.normpath(csv_dir))
    delete_file_name = result_directory + base_filename + '_processed.txt'
    ansible_file_name = result_directory + 'ansible_' + base_filename + '.txt'
    ansible_resources = {'instance-pool': ('oracle.oci.oci_compute_management_instance_pool', 'instance_pool_id'),
                         'instance-configuration': ('oracle.oci.oci_compute_management_instance_configuration',
                                                    'instance_configuration_id'),
                         'instance': ('oracle.oci.oci_compute_instance', 'instance_id'),
                         'volume': ('oracle.oci.oci_blockstorage_volume', 'volume_id'),
                         'load-balancer': ('oracle.oci.oci_loadbalancer_load_balancer', 'load_balancer_id'),
                         'remote-peering-connection': ('oracle.oci.oci_network_remote_peering_connection',
                                                       'remote_peering_connection_id'),
                         'drg-attachment': ('oracle.oci.oci_network_drg_attachment', 'drg_attachment_id'),
                         'drg': ('oracle.oci.oci_network_drg', 'drg_id'),
                         'zone': ('oracle.oci.oci_dns_zone', 'zone_name_or_id'),
                         'resolver-endpoint': ('oracle.oci.oci_dns_resolver_endpoint', 'resolver_id'),
                         'subnet': ('oracle.oci.oci_network_subnet', 'subnet_id'),
                         'route-table': ('oracle.oci.oci_network_route_table', 'rt_id'),
                         'nsg': ('oracle.oci.oci_network_security_group', 'network_security_group_id'),
                         'security-list': ('oracle.oci.oci_network_security_list', 'security_list_id'),
                         'internet-gateway': ('oracle.oci.oci_network_internet_gateway', 'ig_id'),
                         'nat-gateway': ('oracle.oci.oci_network_nat_gateway', 'nat_gateway_id'),
                         'vcn': ('oracle.oci.oci_network_vcn', 'vcn_id'),
                         'view': ('oracle.oci.oci_dns_view', 'view_id')}
    if args.clusters:
        if os.path.isfile(delete_file_name):
            with open(delete_file_name, 'r') as f, open(ansible_file_name, 'w') as a:
                command = ('a ' * 7)
                while True:
                    previous = (command.split()[2], command.split()[5], command.split()[-1])
                    command = f.readline()
                    if not command:
                        break
                    if previous != (command.split()[2], command.split()[5], command.split()[-1]):
                        a.write(','.join(ansible_resources[command.split()[2]] +
                                         (command.split()[5], command.split()[-1])) + '\n')
            args.log.write("Ansible resources file {} is created.\n".format(ansible_file_name))
        else:
            args.log.write("File with OCI delete commands {} not found.\n  "
                           "Be sure to run REPORT playbook first.\n".format(delete_file_name))
    return


main()

#!/usr/bin/env bash

set -euxo pipefail
DELETE_ARGS="--dir result --log result/report.log"

if [ -n "${DELETE_CLUSTERS}" ] ; then
  DELETE_ARGS+=" --clusters ${DELETE_CLUSTERS}"
fi

if [ -n "$1" ] ; then
  if [[ "$1" = "delete" ]] ; then
    python3 oci_ansible_delete_resources.py ${DELETE_ARGS}
  elif [[ "$1" = "report" ]] ; then
    python3 oci_ansible_report.py ${DELETE_ARGS}
  fi
fi

#!/usr/bin/env bash
set -ex

IMAGE_TAG="latest"

if [[ -n "${1}" ]]; then
  IMAGE_TAG="${1}"
fi

IMAGE="nexus.oomasrv.com/helpers/oci-resources"
FULL_IMAGE_NAME="${IMAGE}:${IMAGE_TAG}"
docker buildx build --platform "linux/amd64,linux/arm64" -t "${FULL_IMAGE_NAME}" --push .

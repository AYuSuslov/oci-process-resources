#!/usr/bin/env bash
set -eo pipefail

function print_usage() {
       echo "$0 [OPTIONS VALUE]"
       echo -e "Supported options and values: \n"
       echo "-h               Print usage"
       echo "-c               Compartment to check"
       echo "-r               Region to check"
       echo "-d               Cluster name for deleting, default is NONE"
       echo "-o               Output results path, default is result/"
       echo "-t               Config file section to use (tenancy profile), default is DEFAULT"
       echo "-v               Image tag, default is latest"
       exit
}

PROFILE="DEFAULT"
COMPARTMENT=""
REGION=""
DELETE_CLUSTERS=""
OUTPUT_PATH="result"
DELETE_CLUSTERS=""
OCI_USER_KEY_FILE=""
CONTAINER_USER="oci"
IMAGE_TAG="latest"

while getopts "ht:c:r:d:o:v:" opt ; do
  case $opt in
    h ) print_usage;;
    t ) PROFILE=$OPTARG;;
    c ) COMPARTMENT=$OPTARG;;
    r ) REGION=$OPTARG;;
    d ) DELETE_CLUSTERS+=$OPTARG" ";;
    o ) OUTPUT_PATH=$OPTARG;;
    v ) IMAGE_TAG=$OPTARG;;
    ? ) echo "$OPTARG is unknown option" ; exit 1;;
  esac
done

shift  $((OPTIND-1))

if [ -z "${OCI_CLI_CONFIG_FILE}" ] ; then
  OCI_CONFIG_FILE="${HOME}"/.oci/config
else
  OCI_CONFIG_FILE="${OCI_CLI_CONFIG_FILE}"
fi

while read -r line ; do
  if [[ "${line}" = "[${PROFILE}]" ]] ; then
    IFS="="
    for (( i=1; i <= 5; i++ )) ; do
      read -r name value
      name="$(echo "${name}" | xargs)"
      value="$(echo "${value}" | xargs)"
      case "${name}" in
        user)
          OCI_USER_ID=$value;;
        fingerprint)
          OCI_USER_FINGERPRINT=$value;;
        key_file)
          OCI_USER_KEY_FILE=$value;;
        region)
          OCI_REGION=$value;;
        tenancy)
          OCI_TENANCY=$value ;;
        *)
          echo "OCI config is incorrect" ; exit 1;;
      esac
    done
    break
  fi
done < "${OCI_CONFIG_FILE}"

if [[ -d "${OUTPUT_PATH}" ]] ; then
  rm -f "${OUTPUT_PATH}"/*.csv
else
  mkdir "${OUTPUT_PATH}"
fi

OCI_USER_KEY=$(<"${OCI_USER_KEY_FILE}")

docker run --rm -v ./"${OUTPUT_PATH}"/:/home/"${CONTAINER_USER}"/showoci/result/ -e COMPARTMENT="${COMPARTMENT}" -e REGION="${REGION}" -e OCI_CONFIG_PROFILE="${PROFILE}" -e DELETE_CLUSTERS="${DELETE_CLUSTERS}" \
           -e OCI_USER_ID="${OCI_USER_ID}" -e OCI_USER_FINGERPRINT="${OCI_USER_FINGERPRINT}" -e OCI_REGION="${OCI_REGION}" -e OCI_TENANCY="${OCI_TENANCY}" -e OCI_USER_KEY="${OCI_USER_KEY}" \
           -e UID="$(id -u)" -e GID="$(id -g)" nexus.oomasrv.com/helpers/oci-resources:"${IMAGE_TAG}" "${@}"

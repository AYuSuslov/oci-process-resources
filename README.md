## NOTES

Application runs in the container environment. A user should have docker or other container runtime to run it.

Application collects information about OCI resources in definite region and compartment and outputs it in Excel format tables. 
The user can specify the name of the k8s cluster using one of the arguments, and he gets the list of OCI CLI commands to delete all resources of this particular cluster. 
The user can also run Ansible playbook to delete these resources using Ansible tasks. In this case successfully completed tasks are guaranteeing the correct resources deletion.
More information is [here](https://oomacorp.atlassian.net/wiki/spaces/~624ee7a45f63fd0069b73750/pages/602866010/leaning+the+OCI+resource+garbage)

### BUILD AND PUSH

Build and push image to Nexus repository, default `image_tag_version` is `latest`.

```shell
./build_and_push.sh [<image_tag_version>]
```

### GET SHELL SCRIPT

Pull image from repository and copy OCI shell script from container to host.

```shell
docker run --rm -v $(pwd):/home/oci/target/ --entrypoint cp nexus.oomasrv.com/helpers/oci-resources:<image_tag_version> -p ../shell/oci_run.sh ../target/
```

### REPORT

Run application in the container via Ansible playbook that collects all OCI resources of the choosen compartment and region into a table. 
If the k8s cluster name(s) is(are) specified, the application issues a list of the OCI CLI commands to a text file to delete cluster resources. 
The default arguments are:

`output_path`: `./result/`

`tenancy_profile_name`: `DEFAULT`

`image_tag_version`: `latest`

```shell
./oci_run.sh -c <compartment_name> -r <region_name> -d <k8s_cluster_name_1> [-d <k8s_cluster_name_N> -o <output_path> -t <tenancy_profile_name> -v <image_tag_version>] report
```

### CLEAN

Clean k8s cluster resources from OCI using Ansible playbook. Before running it user can analyse the OCI resources in the list (result text file from previous command) and remove some resources from the deletion list if needed.

```shell
./oci_run.sh -c <compartment_name> -r <region_name> -d <k8s_cluster_name> [-d <k8s_cluster_name_N> -o <output_path> -t <tenancy_profile_name> -v <image_tag_version>] clean
```
